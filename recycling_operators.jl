function recycle_mul(array1::Array{data_type,1}, array2::Array{data_type,1}) where data_type<:Real
    output_array = []
    if length(array1) > length(array2)
        for i = 1:length(array1)
            if i > length(array2)
                j = i%length(array1) + 1
            else
                j = i
            end
            result = array1[i] * array2[j]
            push!(output_array, result)
        end
   else
        for i = 1:length(array2)
            if i > length(array1)
                j = i%length(array2) + 1
            else
                j = i
            end
            result = array2[i] * array1[j]
            push!(output_array, result)
        end
    end
    return output_array
end
function recycle_cat(array1::Array{data_type,1}, array2::Array{data_type,1}) where data_type<:String
    output_array = []
    if length(array1) > length(array2)
        for i = 1:length(array1)
            if i > length(array2)
                j = i%length(array1) + 1
            else
                j = i
            end
            result = array1[i] * array2[j]
            push!(output_array, result)
        end
   else
        for i = 1:length(array2)
            if i > length(array1)
                j = i%length(array2) + 1
            else
                j = i
            end
            result = array2[i] * array1[j]
            push!(output_array, result)
        end
    end
    return output_array
end
function recycle_add(array1::Array{data_type,1}, array2::Array{data_type,1}) where data_type <: Real
    output_array = []
    if length(array1) > length(array2)
        for i = 1:length(array1)
            if i > length(array2)
                j = i%length(array1) + 1
            else
                j = i
            end
            result = array1[i] + array2[j]
            push!(output_array, result)
        end
   else
        for i = 1:length(array2)
            if i > length(array1)
                j = i%length(array2) + 1
            else
                j = i
            end
            result = array2[i] + array1[j]
            push!(output_array, result)
        end
    end
    return output_array
end
